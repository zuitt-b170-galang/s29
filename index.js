let express = require("express");
const app = express();
const port = 3000;

app.use(express.json());
app.use(express.urlencoded({ extended:true }));

app.get("/home", (req, res)=> {
	res.send("Welcome to the home page");
})

let users = [];

app.get("/users", (req, res)=> {
	res.send(users);
})

app.delete("/delete-user", (req, res)=> {
	res.delete(users);
})

app.listen(port, () => console.log(`Server is running at port ${port}`));